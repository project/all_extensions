Allow All File Extensions for file fields

## INTRODUCTION

Usage:
just enable the module and in file extension let it be empty.
after that all file will be accepted.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/all_extensions

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/all_extensions


## REQUIREMENTS

No special requirements.


## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

No configuration is needed.


## MAINTAINERS

Current maintainers:
 * Yuseferi - https://www.drupal.org/u/yuseferi


